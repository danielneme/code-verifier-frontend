import axios from '../utils/config/axios.config';
/**
 * Metodo para loguearse
 * @param {string}email Email para loguearse 
 * @param {string}password Constraseña para loguearse
 * @returns 
 */
export const login=(email:string, password:string)=>{

    // Declaramos el Body para hacer el POST
    let body={
        email: email,
        password: password,
         }
    // Enviamos el POST a nuestro login Endpoint: ->http://localhost:3000/api/auth/login
    return axios.post('/auth/login', body)
        }

        /**
         * Metodo de registro
         * @param {string}name Nombre a regtistrar
         * @param {string}email  Email a registrar
         * @param {string}password Contraseña a registar
         * @param {number}age Edad a registrar
         * @returns 
         */
    export const register=(name:string, email:string, password:string, age:number)=>{

        // Declaramos el Body para hacer el POST
        let body={
            name: name,
            email: email,
            password: password,
            age:age
        }
        // Enviamos el POST a nuestro login Endpoint: ->http://localhost:3000/api/auth/register
        return axios.post('/auth/register', body)

    }