//React Router DOM Imports
import {BrowserRouter as Router, Routes, Route, Navigate, Link} from 'react-router-dom';
import { HomePage } from '../page/HomePage';
import { KatasDetailPage } from '../page/KatasDetailPage';
import { KatasPage } from '../page/KatasPage';
import { LoginPage } from '../page/LoginPage';
import { RegisterPage } from '../page/RegisterPage';

export const AppRoutes = () => {

return(

  <Routes>
  {/*Deficinion de rutas*/}

  <Route path='/' element={<HomePage/>}></Route>
  <Route path='/login' element={<LoginPage/>}></Route>
  <Route path='/register' element={<RegisterPage/>}></Route>
  <Route path='/katas' element={<KatasPage/>}></Route>
  <Route path='/katas/:id' element={<KatasDetailPage/>}></Route>
  {/*Redireccion cuando no se encuentra la pagina */}
  <Route
  path='*'
  element={<Navigate to='/' replace />}>
  </Route>
</Routes>

)}