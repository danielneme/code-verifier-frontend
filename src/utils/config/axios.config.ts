import axios from 'axios';
export default axios.create(
    {
        baseURL:'http://localhost:8080/api', // Ruta que se agregaran los endpoints de nuestro backend.
        responseType:'json',
        timeout:6000
    }
)