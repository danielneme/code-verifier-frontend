import React from 'react';
import{Formik, Form, Field, ErrorMessage} from 'formik';
import * as Yup from 'yup';
import {login} from '../../services/authService';
import { AxiosResponse } from 'axios';
import { useNavigate } from 'react-router-dom';
//Define el esquema de validacion con YUP
const loginSchema=Yup.object().shape(
    {
        email:Yup.string().email('Invalid Email Format').required('Email es requerido'),
        password:Yup.string().required('Password es requerido')
    }
);

//Componente LOGIN
const LoginForm=()=>{

    //Definicion de credenciales iniciales
    const initialCredencials={
        email:'',
        password:''
    }

    let navigate= useNavigate();
    return(
        <div>
            <h4>Login Form</h4>
            {/*Formik para encapsular un Formulario */}
            <Formik
                initialValues={initialCredencials} 
                validationSchema={loginSchema}
                onSubmit={async(values)=>{

                login(values.email, values.password).then(async(response: AxiosResponse)=>{
                    if(response.status===200){
                        if(response.data.token){ 
                            await sessionStorage.setItem('sessionJWTToken', response.data.token)
                            navigate('/');
                        }else{
                            throw new Error('TOKEN de login invalido')
                        }
                      
                      } else{
                      throw new Error('Credenciales invalidas')
                      } 

                 }).catch((error)=> console.error(`[LOGIN ERROR]: Algo salio mal: ${error}`))
              }}
            >
               {
                ({values,touched,errors,isSubmitting,handleChange, handleBlur})=>(
                    <Form>
                        {/*Campo EMAIL */}
                        <label htmlFor='email'>Email</label>
                        <Field id='email' type='email' name='email' placeholder='example@email.com'/>
                        {/*Email Errors*/}
                        {
                            errors.email&&touched.email&&(
                                <ErrorMessage name='email' component='div'></ErrorMessage>
                            )
                        }

                            {/*Campo Contraseña */}
                            <label htmlFor='password'>Password</label>
                        <Field id='password' type='password' name='password' placeholder='example '/>
                        {/*Contraseña Errors*/}
                        {
                            errors.password&&touched.password&&(
                                <ErrorMessage name='password' component='div'></ErrorMessage>
                            )
                        }

                        {/*Boton Submit */}
                        <button type='submit'>Ingresar</button>
                        {/*Mensaje si se logea correctamente */}
                        {
                            isSubmitting ?
                            (<p>Verificando Credenciales...</p>) 
                            :null

                        }
                    </Form>
                )

               } 

            </Formik>
        </div>
    )

}
export default LoginForm;