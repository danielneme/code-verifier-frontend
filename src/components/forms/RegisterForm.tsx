import React from 'react';
import {Formik, Form, Field, ErrorMessage} from 'formik';
import * as Yup from 'yup';
import { register } from '../../services/authService';
import { AxiosResponse } from 'axios';

const RegisterForm=()=>{
    const initialValues={
        name:'',
        email:'',
        password:'',
        confirm:'',
        age:18,
                        }

//Yup Validation Schema
const registerSchema= Yup.object().shape({
    name: Yup.string()
        .min(6, 'Ususario debe tener al menos 6 letras')
        .max(12, 'Usuario debe tener menos de 12 letras')
        .required('El Usuario es requerido'),
    email: Yup.string()
        .email('Formato de correo Invalido')
        .required('El Mail es requerido'),
    password: Yup.string()
        .min(8,'La contraseña debe tener al menos 8 caracteres')
        .required('La contraseña es requerida'),
    confirm:Yup.string()
        .when("password",{
            is:(value:string)=>(value && value.length >0 ? true:false),
                then: Yup.string().oneOf(
                    [Yup.ref("password")], 'Las contraseñas no coinciden'
                )
        })
        .required('Debe confirmar su contraseña'),
    age: Yup.number()
        .min(10, 'Debe ser mayo de 18 años')
        .required('La edad es requerida')
});

return(
    <div>
        <h4>Registro de nuevo usuario</h4>
        {/*Formik wrapper*/}
        <Formik
                initialValues={initialValues} 
                validationSchema={registerSchema}
                onSubmit={async(values)=>{

                    register(values.name, values.email, values.password, values.age).then((response: AxiosResponse)=>{
                        if(response.status===200){
                           console.log('Usuario registrado correctamente!')
                           console.log('response.data')
                           alert('Usuario registrado Correctamente!')
                          } else{
                          throw new Error('Error en el registro')
                          } 
    
                     }).catch((error)=> console.error(`[Register ERROR]: Algo salio mal: ${error}`))
                  }}>

                {
                ({values,touched,errors,isSubmitting,handleChange, handleBlur})=>(
                    <Form>
                        {/*Campo Nombre */}
                        <label htmlFor='name'>Nombre</label>
                        <Field id='name' type='text' name='name' placeholder='Juan Carlos'/>
                        {/*Name Errors*/}
                        {
                            errors.name&&touched.name&&(
                                <ErrorMessage name='name' component='div'></ErrorMessage>
                            )
                        }
                        {/*Campo EMAIL */}
                        <label htmlFor='email'>Email</label>
                        <Field id='email' type='email' name='email' placeholder='example@email.com'/>
                        {/*Email Errors*/}
                        {
                            errors.email&&touched.email&&(
                                <ErrorMessage name='email' component='div'></ErrorMessage>
                            )
                        }

                            {/*Campo Contraseña */}
                            <label htmlFor='password'>Password</label>
                        <Field id='password' type='password' name='password' placeholder='example '/>
                        {/*Contraseña Errors*/}
                        {
                            errors.password&&touched.password&&(
                                <ErrorMessage name='password' component='div'></ErrorMessage>
                            )
                        }
                           {/*Campo de Confirmacion de contraseña */}
                           <label htmlFor='confirm'>Confirm Password</label>
                        <Field id='confirm' type='password' name='confirm' placeholder='confirme su contraseña '/>
                        {/*Confirm Errors*/}
                        {
                            errors.confirm&&touched.confirm&&(
                                <ErrorMessage name='confirm' component='div'></ErrorMessage>
                            )
                        }
                             {/*Campo de Edad */}
                             <label htmlFor='age'>Edad</label>
                        <Field id='age' type='text' name='age' placeholder='su edad '/>
                        {/*Age Errors*/}
                        {
                            errors.age&&touched.age&&(
                                <ErrorMessage name='age' component='div'></ErrorMessage>
                            )
                        }


                        {/*Boton Submit */}
                        <button type='submit'>Ingresar</button>
                        {/*Mensaje si se logea correctamente */}
                        {
                            isSubmitting ?
                            (<p>Verificando Credenciales...</p>) 
                            :null

                        }
                    </Form>
                )

               } 




                </Formik>

    </div>
)

}
export default RegisterForm;