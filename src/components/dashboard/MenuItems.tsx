import React from 'react';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';

//Material Icon Component

import PeopleAltRoundedIcon from '@mui/icons-material/PeopleAltRounded';
import DashboardOutlinedIcon from '@mui/icons-material/DashboardOutlined';
import StarRateOutlinedIcon from '@mui/icons-material/StarRateOutlined';

export const MenuItems = (

        <React.Fragment>
            {/*Dashboard Button de KATAS*/}
            <ListItemButton>
                <ListItemIcon>
                    <DashboardOutlinedIcon/>
                </ListItemIcon>
                <ListItemText primary="Katas"/>
            </ListItemButton>
                 {/*Users*/}
                 <ListItemButton>
                <ListItemIcon>
                    <PeopleAltRoundedIcon/>
                </ListItemIcon>
                <ListItemText primary="Users"/>
            </ListItemButton>
                 {/*Ranking*/}
                 <ListItemButton>
                <ListItemIcon>
                    <StarRateOutlinedIcon/>
                </ListItemIcon>
                <ListItemText primary="Ranking"/>
            </ListItemButton>


        </React.Fragment>

)